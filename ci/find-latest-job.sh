#!/usr/bin/env bash

set -e

project_id=$1
branch_name=$2
job_name=$3

resp=$(mktemp)

# Access token is a protected environment variable in the head.hackage project and
# is necessary for this query to succeed. Sadly job tokens only seem to
# give us access to the project being built.
curl \
  --silent --show-error \
  -H "Private-Token: $ACCESS_TOKEN" \
  "https://gitlab.haskell.org/api/v4/projects/$project_id/pipelines/?ref=$branch_name&scope=finished&per_page=50" \
  > "$resp"

job_ids=$(jq ". | map(.id) " < "$resp")
if [ "$job_ids" = "null" ]; then
  echo "Error finding job $job_name for $pipeline_id in project $project_id:" >&2
  cat "$resp" >&2
  rm "$resp"
  exit 1
else
  for i in $(echo $job_ids | jq '.[]'); do
    if find-job $project_id $i $job_name; then
      exit 0
    fi
  done
  echo "Error finding job $job_name for $branch_name project $project_id:" >&2
  exit 1
fi
